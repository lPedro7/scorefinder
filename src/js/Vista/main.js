

const user = localStorage.getItem("user");

if (user !== null) {

    document.querySelector('li:nth-child(3)').remove();

    const login = document.querySelector('li:nth-child(2) a');

    login.innerHTML = 'Surt';

    document.querySelector('li:nth-child(2)').addEventListener('click', ()=>{
        logOut();
        location.reload();
    })

}else{
    document.querySelector('li:nth-child(2)').addEventListener('click', ()=>{
        openWindow();
    })

    document.querySelector('li:nth-child(3)').addEventListener('click',()=>{
        window.location.replace('./registre.html')
    })
}

function logOut(){
    localStorage.removeItem('user');
}

function openWindow(){
    console.log("open")
    window.open("./login.html", "Login", "_blank");
}


