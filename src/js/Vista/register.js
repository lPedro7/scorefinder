import { register } from '../Servei/registerService.js';

document.querySelector(`#submit`).addEventListener('click',()=>{
   reg()
})
document.querySelector(`#submit`).disabled = true;

document.querySelector(`#repPassword`).addEventListener('input', function () {

    const currentPassword = document.querySelector(`#pass`).value;
    const checkPassword = document.querySelector(`#repPassword`).value;

    if (currentPassword !== checkPassword) {
        document.querySelector(`#submit`).disabled = true;
    } else {
        document.querySelector(`#submit`).disabled = false;
    }

})

function reg(){

    const nom = document.querySelector(`#nomCognom`).value;
    const email = document.querySelector(`#email`).value;
    const password = document.querySelector(`#pass`).value;
    register(nom,email,password);

}
