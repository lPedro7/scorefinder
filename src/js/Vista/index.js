import { getPartitures, addCerca, borra } from '../Servei/partituraService.js';

getPartitures();

let teclas = document.querySelectorAll('.tecla');


teclas.forEach(function (tecla) {
    tecla.addEventListener('click', function () {
        addCerca(tecla.id, tecla.dataset.alteracio)
    })
})


document.querySelector('#borrar').addEventListener('click', function () {
    borra();
})


document.addEventListener('keydown', (event) => {
    const keyName = event.key;


    switch (keyName.toLowerCase()) {
        case 'a': addCerca('do', 'normal'); break;
        case 'z': addCerca('do-sust', 'sostingut'); break;
        case 's': addCerca('re', 'normal'); break;
        case 'x': addCerca('re-sust', 'sostingut'); break;
        case 'd': addCerca('mi', 'normal'); break;
        case 'f': addCerca('fa', 'normal'); break;
        case 'v': addCerca('fa-sust', 'sostingut'); break;
        case 'g': addCerca('sol', 'normal'); break;
        case 'b': addCerca('sol-sust', 'sostingut'); break;
        case 'h': addCerca('la', 'normal'); break;
        case 'n': addCerca('la-sust', 'sostingut'); break;
        case 'j': addCerca('si', 'normal'); break;
        case 'k': addCerca('do_agut', 'normal'); break;
    }
});

const video = document.querySelectorAll('video');
const buttonPip = document.querySelector('#pip');

buttonPip.addEventListener('click', async () => {

    let currentWatching;

    for (let i = 0; i < video.length; i++) {
        if (!video[i].paused) {
            currentWatching = video[i];
        }
    }

    if (document.pictureInPictureElement) {
        document.exitPictureInPicture();
    } else {
        if (document.pictureInPictureEnabled) {
            await currentWatching.requestPictureInPicture();
        }
    }
})

