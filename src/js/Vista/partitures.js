import { getPartitures, deletePartitura } from '../Servei/partituraService.js'

(async function printPartitures() {

    let arrayPartitures = await getPartitures();

    const table = document.createElement('TABLE');
    let tr = document.createElement('TR');
    const thTitol = document.createElement('TH');
    thTitol.innerHTML = 'Titol ';
    const thIdioma = document.createElement('TH');
    thIdioma.innerHTML = 'Idioma Original ';
    const thAccions = document.createElement('TH');
    thAccions.innerHTML = 'Accions ';

    tr.appendChild(thTitol);
    tr.appendChild(thIdioma);
    tr.appendChild(thAccions);
    table.appendChild(tr);

        arrayPartitures.forEach(function(partitura){
            tr = document.createElement('TR');
            const tdTitol = document.createElement('TD');
            const tdIdiomaOriginal = document.createElement('TD');
            const tdAccions = document.createElement('TD');
            let editButton = document.createElement('BUTTON');
            let editIcono = document.createElement('I');
            let borraButton = document.createElement('BUTTON');
            let borraIcono = document.createElement('I');
    
            tdTitol.innerHTML = partitura.titol;
            tdIdiomaOriginal.innerHTML = partitura.idiomaoriginal;
    
            editButton.className = partitura.idPartitura + ' editar';
            editButton.className="fas fa-edit";
            editButton.appendChild(editIcono);
            editButton.innerHTML="Editar";
    
            borraButton.className = partitura.idPartitura + ' esborrar';
            borraButton.className = "fas fa-trash";
            borraButton.appendChild(borraIcono);
            borraButton.innerHTML="Borrar";
    
            tdAccions.appendChild(editButton);
            tdAccions.appendChild(borraButton);
            tr.appendChild(tdTitol);
            tr.appendChild(tdIdiomaOriginal);
            tr.appendChild(tdAccions);
            table.appendChild(tr);
    
    
            editButton.addEventListener('click', function(){
                editPartitura(partitura.idpartitura)
            })
    
            borraButton.addEventListener('click', function(){
                deletePartitura(partitura.idpartitura).then(function(){
                    location.reload();
                })
            })
            
    
        })

       

    
    document.querySelector('main').appendChild(table);

})()

function editPartitura(idPartitura){
    window.open(`./partitura.html?id=${idPartitura}`,"_self")
}

document.querySelector('#novaPartitura').addEventListener('click',function(){
    window.open(`./partitura.html`,"_self");
})

