import { Partitura } from '../Model/Partitura.js';
import { getIdiomes, translateLanguage } from '../Servei/GoogleService.js';
import { getPartituraById, afegeixPartitura } from '../Servei/partituraService.js'
import { Nota } from '../Model/Nota.js'

tinymce.init({
    selector: '#lletraOriginal'
});

const idOriginal = document.getElementsByName('idiomaOriginal')[0];

let arrayIdiomes = [];

(async () => {
    arrayIdiomes = await getIdiomes();
    arrayIdiomes.forEach(function (idioma) {
        let option = document.createElement('OPTION');
        option.value = idioma.codi;
        option.innerHTML = idioma.nom;
        idOriginal.appendChild(option);
    })

})().then(() => {
    if (window.location.search !== '') {
        printPartitura();
    }
})

document.querySelector('#enviar').addEventListener(`click`, function () {


    let checkTitol = false;
    let title = document.getElementsByName('titol')[0].value;


    const pattern3Paraules = /^(\s?\b\w+\b\s?){3}$/;

    if (pattern3Paraules.test(title)) {
        checkTitol = true;
        document.querySelector('#titleError').innerHTML = ""

    } else {
        checkTitol = false;
        document.querySelector('#titleError').innerHTML = `El titol ha de tenir exactament 3 paraules`;
        document.querySelector('#titleError').style = `color:red`
    }

    const notas = getNotas();

    if (checkTitol) {

        const idPartitura = document.querySelector('#partituraId').value;
        const titol = document.getElementsByName('titol')[0].value;
        const idiomaOriginal = document.getElementsByName('idiomaOriginal')[0].value;
        const lletraDesti = document.getElementsByName('traduccioCatala')[0].value;
        const lletraOriginal = tinymce.activeEditor.getContent()
            ;

        console.log(lletraOriginal)

        const part = new Partitura(idPartitura, titol, idiomaOriginal, 'ca', lletraOriginal, lletraDesti, notas);

        afegeixPartitura(part)

    }

})

async function printPartitura() {
    const queryString = window.location.search;

    const urlParams = new URLSearchParams(queryString);

    const idPartitura = urlParams.get('id');
    console.log(idPartitura)
    const partitura = await getPartituraById(idPartitura)

    console.log(partitura)

    document.querySelector('#partituraId').value = idPartitura;

    document.getElementsByName('titol')[0].value = partitura.titol;

    document.getElementsByName('idiomaOriginal')[0].value = partitura.idiomaoriginal;

    tinymce.activeEditor.setContent(`<p>${partitura.lletraoriginal}</p>`)


    document.getElementsByName('traduccioCatala')[0].value = partitura.lletratraduccio;
}


console.log(document.querySelector('#lletraOriginal'))


let actualText = '';

setInterval(function () {

    const newText = tinymce.activeEditor.getContent();

    if (actualText != newText) {
        translateText();
        actualText = newText

    }

}, 500)


async function translateText() {
    const idiomaOriginal = document.getElementsByName('idiomaOriginal')[0].value;
    const text = tinymce.activeEditor.getContent();
    const traduccio = await translateLanguage(idiomaOriginal, text);
    document.getElementsByName('traduccioCatala')[0].value = traduccio.replace(/<[^>]*>?/gm, '');
}

const ordre = 12;
const notas = 11;

for (let i = 0; i < ordre; i++) {
    const div1 = document.createElement('DIV');
    div1.id = `ordre-${i}`
    for (let j = 0; j < notas; j++) {
        const div = document.createElement('DIV');
        div.id = `${i}-${j}`;
        if (j < 10 && j % 2 == 0) {
            div.style.backgroundColor = "black";
            div.style.height = "10px"
        };
        div.setAttribute('data-drop', true)
        div.classList = 'celda ' + i;
        div1.appendChild(div);
    }
    const button = document.createElement('BUTTON');
    button.innerHTML = "Borrar";
    button.id = `celda-${i}`
    div1.appendChild(button)
    document.querySelector('#partitura').appendChild(div1);

}


function drag(ev) {
    ev.dataTransfer.setData('text', ev.target.id);
}

function drop(ev) {

    const posicion = `ordre-${ev.target.id[0]}`
    const div = document.getElementById(posicion)
    const columna = ev.target;
    const drop = columna.getAttribute('data-drop');
    if (drop !== 'false') {

        ev.preventDefault();
        let data = ev.dataTransfer.getData('text');
        let nota = document.createElement('img');
        nota.className = 'notas';
        nota.id = data;
        if (nota.id === 'normal') {
            nota.src = "../assets/notesimg/nota1.png";
        } else {
            nota.src = "../assets/notesimg/nota1sust.png";
        }


        for (let i = 0; i < div.childElementCount - 1; i++) {

            const children = document.getElementById(`${ev.target.id[0]}-${i}`)
            children.setAttribute('data-drop', false)
        }

        nota.draggable = false

        ev.target.appendChild(nota)


    }

}

function allowDrop(ev) {
    ev.preventDefault();
}

const notaNormal = document.querySelector('#normal')
const notaSust = document.querySelector('#sust')

notaSust.addEventListener('dragstart', () => {
    drag(event)
})
notaNormal.addEventListener('dragstart', () => {
    drag(event)
})

const celdas = document.querySelectorAll('.celda');
console.log(celdas)

for (let i = 0; i < celdas.length; i++) {
    celdas[i].addEventListener('drop', () => {
        drop(event)
    })

    celdas[i].addEventListener('dragover', () => {
        allowDrop(event)
    })

}


for (let i = 0; i < 12; i++) {
    document.querySelector(`#celda-${i}`).addEventListener('click', function () {

        for (let j = 0; j < 11; j++) {
            const div = document.getElementById(`${i}\-${j}`);
            if (div.childElementCount > 0) {
                div.removeChild(div.childNodes[0]);
            }
            div.setAttribute('data-drop', true)
        }

    })
}

function getNotas() {

    const notas = [];

    const nota = document.querySelectorAll('.celda>img');

    for (let i = 0; i < nota.length; i++) {

        const parent = nota[i].parentElement;
        const nId = 0;
        let nNom;
        switch (parent.id.split('-')[1]) {
            case "10":
                nNom = 'DO';
                break;
            case "9":
                nNom = 'RE';
                break;
            case "8":
                nNom = 'MI';
                break;
            case "7":
                nNom = 'FA';
                break;
            case "6":
                nNom = 'SOL';
                break;
            case "5":
                nNom = 'LA';
                break;
            case "4":
                nNom = 'SI';
                break;
            case "3":
                nNom = 'DO_AGUT';
                break;
            case "2":
                nNom = 'RE';
                break;
            case "1":
                nNom = 'MI';
                break;
            case "0":
                nNom = 'FA'
                break;
        }


        let nAlteracio;

        if (nota[i].id == "normal") {
            nAlteracio = 'regular'
        } else {
            nAlteracio = 'sharp';
        }

        notas.push({
            note: nNom,
            type: nAlteracio
        })
    }


    return notas;

}


let gumPromise = MediaDevices.getUserMedia({ audio: true, video: true });

gumPromise.then(function(mediaStream) {
    let video = document.querySelector('#enviar');
    video.src=window.URL.createObjectURL(mediaStream);
    video.onloadedmetadata= function(e) {
        
    }
})