import { Idioma } from '../Model/Idioma.js';

export async function getIdiomes() {

    let x = await fetch("http://server247.cfgs.esliceu.net/piano/google/translate/languages", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-urlencoded',
            'Authorization': localStorage.getItem('token')
        }
    }).then(r => r.json());

    let arrayIdiomes = [];
    for (let i = 0; i < x.length; i++) {
        arrayIdiomes.push(new Idioma(x[i].code, x[i].name))
    }

    return arrayIdiomes;
}

export async function translateLanguage(languageFrom, text) {

    let translate = {
        languageFrom: languageFrom,
        languageTo: 'ca',
        text: text
    }


    let x = await fetch(`http://server247.cfgs.esliceu.net/piano/google/translate`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': localStorage.getItem('token')

        },
        body: JSON.stringify({
            languageFrom: translate.languageFrom,
            languageTo: translate.languageTo,
            text: translate.text
        })
    }).then(r => r.text());


    return x;
}