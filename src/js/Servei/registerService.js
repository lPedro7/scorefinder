import { User } from '../Model/User.js';
import { notification } from './partituraService.js';

export async function register(nom, email, password) {


    const user = new User(nom, email, password);


    let register = await fetch('https://server247.cfgs.esliceu.net:24743/piano/auth/signup', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            usuari: {
                email: user.usuari.email,
                nom: user.usuari.nom
            },
            password: user.password
        })
    }).then(register => register.json())

    notification(register.notifyMessage)
}

export async function login(username, password) {

    console.log({
        usuari: username,
        password: password
    })

    const login = await fetch('https://server247.cfgs.esliceu.net:24743/piano/auth/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: `{ usuari : ${username}, password: ${password}}`
    })


    console.log(login)

    if (login.ok) {
        const token = await login.text()

        localStorage.setItem('token', token)

        console.log(login)

        getUser();


    } else {
        notification(JSON.parse(await login.text()).notifyMessage)
    }




}

async function getUser() {


    const token = localStorage.getItem('token')

    const x = await fetch('https://server247.cfgs.esliceu.net:24743/piano/user/get', {
        method: 'POST',
        headers: {
            'Authorization': token
        },
    }).then(x => x.json());

    localStorage.setItem('user', JSON.stringify(x))

    window.opener.location.reload();
    window.close();


}
