import { Nota } from '../Model/Nota.js';
import { Partitura } from '../Model/Partitura.js';

let arrayCerca = [];
let arrayPartitures = [];

export async function getPartitures() {

    let x = await fetch("http://server247.cfgs.esliceu.net/piano/score/list", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-urlencoded',
            'Authorization': localStorage.getItem('token')
        }
    }).then(r => r.json())



    for (let i = 0; i < x.length; i++) {
        arrayPartitures.push(new Partitura(x[i].idpartitura, x[i].titol, x[i].idiomaoriginal, x[i].idiomatraduccio, x[i].lletraoriginal, x[i].lletratraduccio, x[i].notes))
    }

    for (let i = 0; i < arrayPartitures.length; i++) {
        arrayPartitures[i].notes = ordenaNotes(arrayPartitures[i].notes)
    }

    return x


}

export function addCerca(nom, alteracio) {
    let newNota = new Nota(0, nom, alteracio, arrayCerca.length);
    arrayCerca.push(newNota);


    playNote(newNota);

    document.querySelector('#cercaNotes').innerHTML += newNota.nom + ' ';


    cercador();
}

function cercador() {
    document.querySelector('#resultat').innerHTML = ''

    let stringCerca = concatArray(arrayCerca);
    let stringNotesPartitures = [];


    for (let i = 0; i < arrayPartitures.length; i++) {

        stringNotesPartitures.push(concatArray(arrayPartitures[i].notes))
    }


    console.log(arrayPartitures)

    for (let i = 0; i < arrayPartitures.length; i++) {

        if (stringNotesPartitures[i].includes(stringCerca)) {
            const div = document.createElement('DIV');
            div.id = `partitura${arrayPartitures[i].id}`;
            div.innerHTML = arrayPartitures[i].titol;
            const button = document.createElement('BUTTON');
            button.innerHTML = 'Reprodueix Cançó';
            button.onclick = (function () {
                playSong(arrayPartitures[i]);
            })
            const letra = document.createElement('BUTTON');
            letra.innerHTML = 'Lletra';
            const copyOriginal = document.createElement('BUTTON');
            copyOriginal.innerHTML = "Copy to Clipboard";

            copyOriginal.addEventListener('click', function () {
                navigator.clipboard.writeText(document.querySelector('#original').innerHTML)
                notification('Text original copiat correctament')
            })

            const copyTranslate = document.createElement('BUTTON');
            copyTranslate.innerHTML='Copy to Clipboard'
            copyTranslate.addEventListener('click',function(){
                navigator.clipboard.writeText(document.querySelector('#traduccio').innerHTML)
                notification('Text traduit copiat correctament')

            })

            letra.onclick = (function () {

                console.log(arrayPartitures[i])

                const modalLletra = document.querySelector('#lletra');
                const span = document.getElementsByClassName('close')[0];
                modalLletra.style.display = "block";
                
                const text = document.querySelector('#textLetra');
                text.innerHTML="";
                const h1 = document.createElement('H1');
                h1.innerHTML = arrayPartitures[i].titol;

                const divOriginal = document.createElement('DIV');
                const pLetra = document.createElement('P');
                pLetra.innerHTML=arrayPartitures[i].lletraOriginal;
                pLetra.id = 'original'

                const divTranslate = document.createElement('DIV');
                const pTraduccio = document.createElement('P');
                pTraduccio.innerHTML=arrayPartitures[i].lletraDesti;
                pTraduccio.id = 'traduccio';




                text.appendChild(h1);
                divOriginal.appendChild(pLetra)
                divOriginal.appendChild(copyOriginal)
                text.appendChild(divOriginal);

                divTranslate.appendChild(pTraduccio);
                divTranslate.appendChild(copyTranslate);
                text.appendChild(divTranslate)



                span.onclick = (function () {
                    modalLletra.style.display = "none"
                })

            })


            div.appendChild(button)
            div.appendChild(letra)
            document.querySelector('#resultat').appendChild(div);
        }
    }

}

function concatArray(array) {
    let string = '';
    for (let i = 0; i < array.length; i++) {
        string += array[i].nom;
    }
    return string;
}

function playSong(arrayPartitura) {

    let arrayNotas = arrayPartitura.notes;


    for (let i = 0; i < arrayNotas.length; i++) {
        arrayNotas[i] = new Nota(0, arrayNotas[i].nom, arrayNotas[i].alteracio, arrayNotas[i].ordre);
    }
    let i = 0;
    let interval = setInterval(function () {
        let notaAudio;
        if (arrayNotas[i].nom == 'DO_AGUT') {
            notaAudio = ` ../assets/audio/do7.ogg`
        } else {
            notaAudio = `../assets/audio/${arrayNotas[i].nom}.ogg`
        }
        const sound = new Audio(notaAudio);
        sound.play();
        i++;
        document.querySelector(`#partitura${arrayPartitura.id}>button`).innerHTML = arrayNotas.length - i;

        if (i == arrayNotas.length) {
            document.querySelector(`#partitura${arrayPartitura.id}>button`).innerHTML = `Reprodueix cançó`;
            clearInterval(interval);

        }

    }, 1000)
}

function playNote(nota) {
    let notaAudio;
    if (nota.nom == `DO_AGUT`) {
        notaAudio = `../assets/audio/do7.ogg`
    } else {
        notaAudio = `../assets/audio/${nota.nom}.ogg`
    }

    const sound = new Audio(notaAudio);
    sound.play();
}

function ordenaNotes(arrayNotes) {

    let newArray = [];
    for (let i = 0; i < arrayNotes.length; i++) {

        newArray[arrayNotes[i].ordre - 1] = arrayNotes[i]

    }

    return newArray;
}

export function borra() {
    arrayCerca = [];
    document.querySelector('#cercaNotes').innerHTML = '';
    document.querySelector('#resultat').innerHTML = '';
}

export async function getPartituraById(idpartitura) {
    let x = await fetch("http://server247.cfgs.esliceu.net/piano/score/get", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': localStorage.getItem('token')
        },
        body: JSON.stringify({ id: idpartitura })
    }).then(r => r.json());

    return x;
}

export async function afegeixPartitura(partitura) {

    console.log(partitura)

    let score = {
        idpartitura: partitura.id,
        name: partitura.titol,
        partituraoriginal: partitura.lletraOriginal,
        partituratraduccio: partitura.lletraDesti,
        idiomaoriginal: partitura.idiomaOriginal,
        idiomatraduccio: 'ca',
        notes: partitura.notes
    }

    console.log(score)

    let x = await fetch("http://server247.cfgs.esliceu.net/piano/score/save", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': localStorage.getItem('token')
        },
        body: JSON.stringify({ score })
    }).then(x => x.json())
    notification(x.notifyMessage);
}


export async function deletePartitura(idpartitura) {
    let borra = confirm(`Vols esborrar aquesta partitura?`);
    if (borra) {
        let x = await fetch("http://server247.cfgs.esliceu.net/piano/score/delete", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem('token')
            },
            body: JSON.stringify({ id: idpartitura })
        }).then(x => x.json());
        notification(x.notifyMessage)
    }

}

export function notification(message) {
    if (window.Notification && Notification.permission !== "denied") {
        Notification.requestPermission(function (status) {  // status is "granted", if accepted by user
            var n = new Notification(`Score Finder`, {
                body: `${message}`,
                icon: '../assets/img/liceu.jpg'
            });
        });
    }
}